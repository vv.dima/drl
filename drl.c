// ATMEL ATTINY13
//
//                   +-\/-+
// Reset       PB5  1|    |8  VCC
//             PB3  2|    |7  PB2
//             PB4  3|    |6  PB1
//             GND  4|    |5  PB0
//                   +----+ 



#define F_CPU 9600000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define NEG_IN PB3 //brake lamp
#define POS_IN PB4 //head lamp
#define LIGHT_PIN  PB1

#define true 1
#define false 0

#define INDELAY 5 
int neg_input_high(){
	int val;
	static int stored_val;
	static int timer = 0;
	val = PINB & (1<<NEG_IN);
	if (stored_val != val){
		if (!timer)
			timer  = INDELAY;
		if (timer)
			timer--;		
		if (!timer)
			stored_val = val;
	}else{
		timer = 0;
	}
	return stored_val;
}

int pos_input_high(){
	int val;
	static int stored_val;
	static int timer = 0;
	val = PINB & (1<<POS_IN);
	if (stored_val != val){
		if (!timer)
			timer  = INDELAY;
		if (timer)
			timer--;	
		if (!timer)
			stored_val = val;
	}else{
		timer = 0;
	}
	return stored_val;
}

void set_light(int light){
	static int start = 0;
	if (light){
		TCCR0A |= (1 << COM0B1);
		if (start){
			OCR0B = 0xFF;
			start--;
			return;
		}	
//		if (OCR0B>0x7D){
//		if (OCR0B>0x69){
		if (OCR0B>0x55){
			OCR0B -= 10; 
		}
	} else {
		start = 4;
		PORTB &= ~(1<<LIGHT_PIN);
		OCR0B=0x00;
		TCCR0A &= ~(1 << COM0B1);
	}
}

void PWM_init(){
	// Set Timer 0 prescaler to clock/64.
	// At 4.8 MHz this is 75 KHz.
	// See ATtiny13 datasheet, Table 11.9.
	TCCR0B |= (1 << CS01) | (1 << CS00) ;

	// Set to 'Fast PWM' mode
	TCCR0A |= (1 << WGM01) | (1 << WGM00);

	// Clear OC0B output on compare match, upwards counting.
	//TCCR0A |= (1 << COM0B1);// this will switch on later in set_light

	TCNT0=0x00;
	OCR0A=0x00;
	OCR0B=0x00;
}

void pin_init(){
	DDRB = 0xFF;
	DDRB &= ~(1<<NEG_IN) & ~(1<<POS_IN);
	PORTB |= (1<<NEG_IN);
	set_light(false);
}

int main(void) {
	int brake_unset = false;
	pin_init();
	PWM_init();
	while (1){
		_delay_ms(5);
		if (neg_input_high())// may be inputs are misswaped
			brake_unset = true;
		if ((pos_input_high()) || (!brake_unset))
			set_light(false);
		else
			set_light(true);
	}
} 
